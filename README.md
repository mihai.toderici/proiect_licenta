link catre repo: https://gitlab.upt.ro/mihai.toderici/proiect_licenta/

Pentru a putea compila cod_esp32.ino trebuie folosit Arduino IDE.
Bibliotecile necesare sunt gasite pe linkurile:

- https://github.com/milesburton/Arduino-Temperature-Control-Library
- https://github.com/knolleary/pubsubclient/releases/tag/v2.8
- https://github.com/PaulStoffregen/OneWire


Biblioteca WiFi.h vine preinstalata cu Arduino IDE.

Pentru accesarea aplicatiei Ignition SCADA vor fi folosite urmatoarele credentiale:
- username = admin
- password = admin

