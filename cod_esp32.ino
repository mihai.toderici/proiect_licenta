
#include <WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>            
#include <DallasTemperature.h>  


#define p1PIN 17        
#define p2PIN 16        
#define p3PIN 4        
#define sus1trigPIN 32  
#define sus1echoPIN 36  
#define sus2trigPIN 33  
#define sus2echoPIN 39  
#define sus3trigPIN 25  
#define sus3echoPIN 34  
#define tdsPIN 26       
#define tempPIN 27      


#define wifi_ssid "TP-LINK_2F5C"
#define wifi_password "p@$l1nkNETTP"
#define mqtt_server "192.168.0.101"


#define pompa1_topic "releu/pompa1"
#define pompa2_topic "releu/pompa2"
#define pompa3_topic "releu/pompa3"
#define nivel1_topic "senzor/nivel1"
#define nivel2_topic "senzor/nivel2"
#define nivel3_topic "senzor/nivel3"
#define tds_topic "senzor/tds"
#define temperatura_topic "senzor/temperatura"

OneWire oneWire(tempPIN);
DallasTemperature sensors(&oneWire);

WiFiClient espClient;
PubSubClient client(espClient);

float tdsValue = 0;
char strValoare[10];
float t = 0;
float voltaj=0;
float coefCompensare = 0;
float voltajCompensat = 0;

byte* comanda;
boolean Rflag = false;
String Topic;

float calc_distanta(int trig, int echo) {

  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  return ((pulseIn(echo, HIGH)) * .0343) / 2;
}

void comparare(int pin) {

  if (comanda[0] == '0') {
    digitalWrite(pin, 1);
  } else if (comanda[0] == '1') {
    digitalWrite(pin, 0);
  }
}

void setup_wifi() {
  delay(10);
  

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  WiFi.setSleep(false);
}

void reconnect() {
  

  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    if (client.connect("client_esp32")) {
      Serial.println("connected");
      client.subscribe(pompa1_topic);
      client.subscribe(pompa2_topic);
      client.subscribe(pompa3_topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {

  Topic = topic;
  Rflag = true;
  comanda = (byte*)malloc(length);
  if (comanda != NULL) {
    for (int i = 0; i < length; i++) {
      comanda[i] = payload[i];
    }
  }
}

void setup() {

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  pinMode(sus1trigPIN, OUTPUT);
  pinMode(sus1echoPIN, INPUT);

  pinMode(sus2trigPIN, OUTPUT);
  pinMode(sus2echoPIN, INPUT);

  pinMode(sus3trigPIN, OUTPUT);
  pinMode(sus3echoPIN, INPUT);


  sensors.begin();

  pinMode(tdsPIN, INPUT);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();


  if (Rflag) {

    if (Topic.equals(pompa1_topic))
      comparare(p1PIN);

    if (Topic.equals(pompa2_topic))
      comparare(p2PIN);

    if (Topic.equals(pompa3_topic))
      comparare(p3PIN);

    free(comanda); 
    Rflag = false;
  }
  sensors.requestTemperatures();
  t = sensors.getTempCByIndex(0);

  voltaj = analogRead(tdsPIN) * (float)3.3 / 4096.0;
  coefCompensare = 1.0 + 0.02 * (t - 25.0);
  voltajCompensat = voltaj / coefCompensare;
  tdsValue = (133.42 * voltajCompensat * voltajCompensat * voltajCompensat - 255.86 * voltajCompensat * voltajCompensat + 857.39 * voltajCompensat) * 0.5;

  dtostrf(calc_distanta(sus1trigPIN, sus1echoPIN), 1, 1, strValoare);
  client.publish(nivel1_topic, strValoare, true);

  dtostrf(calc_distanta(sus2trigPIN, sus2echoPIN), 1, 1, strValoare);
  client.publish(nivel2_topic, strValoare, true);

  dtostrf(calc_distanta(sus3trigPIN, sus3echoPIN), 1, 1, strValoare);
  client.publish(nivel3_topic, strValoare, true);

  dtostrf(t, 1, 1, strValoare);
  client.publish(temperatura_topic, strValoare, true);

  dtostrf(tdsValue, 1, 0, strValoare);
  client.publish(tds_topic, strValoare, true);
  
}